require 'spec_helper_acceptance'

describe 'devopsdays' do
  it 'applies' do
    pp = <<-MANIFEST
      class { 'devopsdays': }
    MANIFEST

    apply_manifest(pp, catch_failures: true)
    # apply_manifest(pp, catch_changes: true)
  end

  describe service('nginx') do
    it { is_expected.to be_running }
    it { is_expected.to be_enabled }
  end

  os_facts = fact('os')

  case os_facts['name']
  when 'Ubuntu'
    mysql_svc = 'mysql'
  when %r{^(RedHat|CentOS)$}
    mysql_svc = 'mariadb'
  end

  fpm_svc = case os_facts['release']['major']
            when '18.04'
              'php7.2-fpm'
            when '16.04'
              'php7.0-fpm'
            else
              'php-fpm'
            end

  describe service(mysql_svc) do
    it { is_expected.to be_running }
    it { is_expected.to be_enabled }
  end

  describe service(fpm_svc) do
    it { is_expected.to be_running }
    it { is_expected.to be_enabled }
  end
end
